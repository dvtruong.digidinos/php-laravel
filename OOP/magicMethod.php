<?php
  class myClass {
    public $prop = "I'm a class property!";
    // magic method là hàm chạy ngay khi class được khởi tạo
    public function __construct() {
      echo 'The class"',__CLASS__,'"was initiated!<br>';
    }
    public function __destruct()
    {
        echo 'The class "', __CLASS__, '" was destroyed.<br>';
    }
    public function setProperty($newVal) {
      $this->prop = $newVal;
    }
    public function getProperty() {
      return $this->prop."<br>";
    }
  }
  // __CLASS__ trả về tên class mà nó được gọi
  $obj = new myClass();
  echo $obj->getProperty();
  echo 'Set property of class<br>';
  $a = $obj->setProperty('I am not a class property!<br>');
?>