<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
  echo '<h1>Bài 05: Lệnh break, continue, die, exit trong PHP.</h1>';
  echo '<h3>1.Break</h3>';
  // Break
  $arr = array(1,2,3,4,5,6,7,8,9,10,11,12,13);
  for ($i = 1; $i <= 100; $i++) {
    echo $i. " ";
    echo '<br>';
    if ($i == 9) {
      echo 'Nhảy ra khỏi vòng for';
      break;
    }
    // Continue
    echo '<h3>2.Continue</h3>';
    if ($i == 4) {
      echo 'Đây là phần tử thứ'.$i.' có giá trị:', $arr[$i];
      continue;
    }
  }

  echo '<br>';

  // die
  $a = 100;
  $b = 200;
  if ($a > $b) {
    echo 'a > b';
  } else {
    echo 'a < b';
  }
  die(); // dừng chương trình tại đây
  exit(); // tương tự với die
  if($a == $b) {
    echo 'a = b';
  } else {
    echo 'a # b';
  }

  ?>
</body>
</html>