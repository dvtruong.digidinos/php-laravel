<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
    echo '<h1>Bài 4: For, while and foreach trong PHP</h1>';


    echo '<h3>1. For trong PHP</h3>';
    // Vòng lặp for
    $arr = array('1','2', '3', '4', '5', '6', '7');
    for($i = 0; $i < 7; $i++) {
      echo $arr[$i],'-';
    }
    
    // Vòng lặp lồng nhau
    echo '<br>';
    echo 'For lồng nhau';
    $arr = [1,2,3,4,5,6,7,8,9];
    for($i = 0; $i < 9; $i++) {
      for ($j = 9; $j >= $i; $j--) {
        echo $j;
      }
      echo '<br>';
    }

    // for cho mảng 
    $arr = array(1,2,3,4,5,6,7,8,9,10,11,12);
    for ($i = 0;$i < count($arr); $i++) {
      echo $arr[$i],'-';
    }

    echo '<br>';
    echo '<h3>2.While và while do trong PHP</h3>';
    echo 'while với điều kiện ';
    $a = [1,2,3,4,5,6,7,8,9,10];
    $i = 10;
    // dùng while để lặp
    while($i <= 10) {
      echo $i. '-';
      $i++;
    }

    $i = 0;
    $j = 30;
    while($i < 100 && $j > 5) {
      $i++;
      $j -= 2; //
      echo '<br>giá trị của j:', $j;
    }
    
    echo '<br><br>';
    echo 'do while trong PHP<br>';
    $i = 1;
    do {
      echo $i,',';
      $i++;
    } while ($i < 100);
    echo '<br>';

    echo '<h3> 3. Truy xuất mảng với foreach</h3>';
    $arr = [1,2.3,5,6,77,88,132,43434,1231231,1231231];
    foreach($arr as $key => $value) {
      echo $value, '-';
    }
    echo '<br>';
    foreach($arr as $key => $value) {
      echo $key. '=> '.$value .'<br>';
    }

    echo '<br>';
    $arr = array(
      'sv001' => 'SINH VIEN 1',
      'sv002' => 'SINH VIEN 2',
      'sv003' => 'SINH VIEN 3',
      'sv004' => 'SINH VIEN 4',
      'sv005' => 'SINH VIEN 5',
      'sv006' => 'SINH VIEN 6'
    );

    foreach($arr as $ten) {
      echo $ten. '<br>';
    }
    
  ?>
</body>
</html>