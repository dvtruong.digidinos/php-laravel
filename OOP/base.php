<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
  // object and class trop PHP
    class dongVat {
      public $ten;
      public $soChan;
      private $canNang;

      function dongVat($ten, $soChan) {
        $this->ten = $ten;
        $this->soChan = $soChan;
      }

      public function setCanNang($cn) {
        $this->canNang = $cn;
      }

      public function getCanNang() {
        return $this->canNang;
      }
    }
    $conVit = new dongVat('donan', 2);
    echo 'Tên con vịt là:'.$conVit->ten.'<br>';
    echo  'Số chân là:'. $conVit->soChan.'<br>';
    $conVit->setCanNang(50);
    $can = $conVit->getCanNang();
    echo 'Cân nặng:'.$can;
  ?>
</body>
</html>