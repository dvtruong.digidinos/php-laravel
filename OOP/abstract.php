<?php
//Lớp abstract là một lớp trừu tượng được xây dựng lên như một lớp cha cho các lớp con có cùng bản chất
  abstract class Template
  // Định nghĩa và có thể thực thi được
  {
      public function make()
      {
          return $this
              ->addHotWater()
              ->addSugar()
              ->addPrimaryToppings()
              ->addPrimaryWater()
              ->addMilk();
      }

      protected  function  addHotWater()
      {
        var_dump('Pour Hot water into cup<br>');
        return $this;
      }

      protected  function addSugar()
      {
        var_dump('Add proper amount of sugar<br>');
        return $this;
      }

      protected function addMilk()
      {
        var_dump('Add proper amount of Milk<br>');
        return $this;
      }

      // abstract function không thể khai báo, khởi tạo đối tượng trọng function này
      protected abstract function addPrimaryToppings();
      protected abstract function addPrimaryWater();
  }

  // Tất cả các class muốn kế thừa lớp abstract phải sử dụng từ khóa extends
  // Tất cả các class kế thừa từ lớp abstract phải override lại lớp để viết tiếp cho 2 abstract function
  class Tea extends Template
    {
        public function addPrimaryToppings()
        {
          var_dump('Add proper amount of tea<br>');
           return $this;
        }
        public function addPrimaryWater()
        {
          var_dump('Add proper amount of water<br>');
          return $this;
        }
    }

    $tea = new Tea();
    $tea->make();

    class Coffee extends Template
    {
        public function addPrimaryToppings()
        {
          var_dump('Add proper amount of Coffee<br>');
          return $this;
        }

        public function addPrimaryWater()
        {
          var_dump('Add proper amount of water<br>');
          return $this;
        }
    }

    $coffee = new Coffee();
    $coffee->make();

    class Water extends Template {
      public function addPrimaryToppings() {
        var_dump('Add proper amount of Water<br>');
        return $this;
      }
      public function addPrimaryWater()
      {
        var_dump('Add proper amount of water<br>');
        return $this;
      }
    }

    $water= new Water();
    $water->make();
?>