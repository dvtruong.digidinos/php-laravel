<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
    // CÁC KIỂU DỮ LIỆU TRONG PHP
    echo '<h1>Bài 02: Các kiểu dữ liệu trong PHP</h1>';
    echo '<h2>1. Kiểu dữ liệu INT</h2>';
    $a = 100; // Kiểu int
    $b = 55.55; // Kiểu float
    $c = 'Chào các bạn đến với khóa học lập trình PHP'; //kiểu string
    $d = true; // kiểu boolean
    echo 'Khai báo kiến a = 100 <br>';
    echo 'Kiểu dữ liệu của a là: ';
    var_dump($a);
    echo '<br>';
    echo 'Kiểu dữ liệu của b là: ';
    var_dump($b);
    echo '<br>';
    echo 'Kiểu dữ liệu của c là: ';
    var_dump($c);
    echo ' <br>';
    echo 'Kiểu dữ liệu của d là: ';
    var_dump($d);

    // Ép kiểu int
    echo '<br><br>';
    echo '<h2>2.Ép kiểu dữ liệu sang int </h2><br>';
    $tuoi = '22';
    $tuoi = (int)$tuoi;
    echo '<br> kiểu dữ liệu của biến tuoi là:';
    var_dump($tuoi);

    echo '<br>';
    $a = '1233';
    $a = 1234;
    $c = $a + $b;
    var_dump(is_int($c));
    var_dump(is_int($a));


    echo '<br>';
    echo '<h2>3.Kiểu Boolean</h2>';
    // Kiểu dữ liệu Boolean
    $is_admin = false;
    if ($is_admin) {
      echo 'is Admin';
    } else {
      echo 'is not Admin';
    }
    echo '<br>';

    // Ép kiểu boolean

    echo '<h2>4.Ép Kiểu Boolean</h2>';
    $bool = 1;
    $bool = (bool)$bool;
    $bool = (boolean)$bool;
    echo 'cho bool =1';
    echo 'Giá trị cỉa bool là:', $bool;

    // kiểm tra 1 biến là boolean
    echo '<h2>5.Kiểm Tra Kiểu Boolean</h2>';
    $bool_1 = true;
    $bool_2 = 'Xin Chào Các Bạn!';
    var_dump(is_bool($bool_1));
    var_dump(is_bool($bool_2));

    echo '<br>';
    // Kiểu số thực
    $a = 12.3;
    echo 'Giá trị của $a là :', $a;
    echo '<br>';
    echo 'Kiểu giá trị của $a là: ';
    var_dump(is_float($a));

    echo '<br>';
    echo '<h2>6.Ép Kiểu Float</h2>';
    $a = 100;
    echo 'cho a = 100 <br>';
    echo 'Ep $a thành kiểu boolean <br>';
    $a = (float)$a; // biến $a lúc này là kiểu số thực
    echo 'Kiểu giá trị của a là:';
    var_dump($a);

    //Kiểu chuỗi

    $a = '123';
    $a = 1234;
    $a = (string)$a; //Ép kiểu string

    //Mảng trong PHP

    echo '<br>'
    // kiểu 1
    echo '<h2>7. Array trong PHP';

    echo '<br><h3> 7.1. Mảng chỉ mục<h3>';
    $arr = array('Nguyễn Văn A', 'Nguyễn Văn B');

    print_r($arr);
    // kiểu 2
    $arr = array(
      0 => 'Nguyễn Văn A',
      1 => 'Nguyễn Văn B'
    );
    print_r($arr);
    // kiểu 3
    $arr[0] =  'Nguyễn Văn A';
    $arr[1] = 'Nguyên Văn B';

    print_r($arr);
    echo '<br>';
    
    echo '<br><h3> 7.2. Mảng kết hợp<h3>';
    //Kiểu 1
    echo "kiểu ",'$arr'," = array('key' => 'value')"
    $arr = array(
      'sinh_vien_a' => 'NGuyễn Văn A',
      'sinh_vien_b' => 'Nguyễn Văn B'
    )
    print_r($arr);
    echo '<br>'
    // Kiểu 2
    echo "Kiểu", '$arr',"['sinh_vien_a'] = 'Nguyễn Văn A'";
    $arr['sinh_vien_a'] = 'Nguyễn Văn A';

    // mảng 1 chiều
    echo '<br>'
    echo '<h2>8. Mảng nhiều chiều</h2>'
    $arr = array();
    $arr[0][1] = 123;
    $arr[0][2] = 234;
    echo $arr[0][1];
    print_r($arr)

    echo '<br>'
    echo 'Kiểm tra kiểu mang sử dụng hàm is_arry()'
    $arr = [1, 2, 3, 4];
    var_dump(is_array($arr));

    // Kiểu giá trị null trong PHP
    echo '<h2>9. Kiểu dữ liệu null trong PHP</h2>';
    $a = null;
    $b = (int)$a; // Biến $b có giá trị là 0
    $c = (string)$b; // Biến $c có giá trị rông ('')
    $d = (bool)$c; // Biến 4d có giá trị FALSE
  ?>
</body>
</html>