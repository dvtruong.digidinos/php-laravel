<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1>Bài 12: Lệnh require - require_once - include - include_once trong PHP </h1>
    <?php
      // require 'C:\xampp\htdocs\php-laravel\training-php-laravel\import.php'; // lấy nhiều lần
      // require_once 'C:\xampp\htdocs\php-laravel\training-php-laravel\import.php'; // chỉ lấy 1 lần với 1 file
      include 'C:\xampp\htdocs\php-laravel\training-php-laravel\import.php'; // giống với require
      include_once 'C:\xampp\htdocs\php-laravel\training-php-laravel\import.php'; // giống với require one
      showMessage();
    ?>
  </div>
</body>
</html>