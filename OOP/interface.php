<?php
  // interface: không phải là một class, interface được mô tả như một bản thiết kế cho các class con có chung cách hoạt động
  interface template {
    //Chỉ được định nghĩa và Không thể thực thi trong interface
    // Không có phạm vi của phương thức
    public function addSugar();
    public function addWater();
    public function addTea();
    public function addMilk();
  }

  class Tea implements template{
    public function make() {
      return $this
                ->addSugar()
                ->addWater()
                ->addTea()
                ->addMilk();
    }
    // Các lớp implements interface phải orverride các hàm trong interface
    public function addSugar() {
      var_dump('Add proper amount of sugar<br>');
      return $this;
    }

    public function addWater() {
      var_dump('Add proper amount of water<br>');
      return $this;
    }
    public function addTea() {
       var_dump('Add proper amount of Tea<br>');
      return $this;
    }
    public function addMilk() {
       var_dump('Add proper amount of Milk<br>');
      return $this;
    }
  }
  $tea = new Tea();
  $show = $tea->make();
?>