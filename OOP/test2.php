<?php
class A {
    public static function returnClassName() {
        return __CLASS__;
    }
    public static function printClassName() {
      return self::returnClassName();
    }
}

class B extends A {
    public static function returnClassName() {
        return __CLASS__;
    }
}

echo B::printClassName();
?>