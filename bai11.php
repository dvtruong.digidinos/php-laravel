<!DOCTYPE html>
<html lang="en">
<head>
  <?php session_start();
    if (isset($_POST['save-session'])) {
      $_SESSION['name'] = $_POST['username'];
      $_SESSION['pass'] = $_POST['password'];
    }
  ?>
  <?php 
     if (isset($_POST['delete-session'])){
        unset($_SESSION['name']);
        unset($_SESSION['pass']);
  
        session_destroy();
      }
    ?>
  <?php
    if (isset($_POST['save-cookie'])) {
      setcookie('name',$_POST['fullname'], time() + 3600);
      setcookie('age',$_POST['age'], time() + 3600);
    } else if(isset($_POST['delete-cookie'])) {
      setcookie('name',$_POST['fullname'], time() - 3600);
      setcookie('age',$_POST['age'], time() - 3600);
    }
  ?>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1>Bài 11: Session và cookie trong PHP</h1>
    <h3>1. Session trong PHP </h3>
    <h3>
      <?php 
        if (isset($_SESSION['name'])) {
          echo 'Tên đăng nhập là:'. $_SESSION['name'].'<br>';
          echo 'Pass là:'.$_SESSION['pass'];
        }
        else {
          echo '<b> Session đã bị xóa</b>';
        }
      ?> 
    </h3>
    <form method ="POST" action="">
        <input type="text" name="username" placeholder="Username"><br>
        <input type="password" name="password" placeholder="Password"><br>
        <button type="submit" name='save-session'>Send</button>
        <button type="submit" name='delete-session'>Delete</button>
    </form>

    <h3>2. Cookie trong PHP</h3>
    <h4>
        <?php
          if(isset($_COOKIE['name'])) {
            echo 'Tên của bạn là:'. $_COOKIE['name'] .'<br>';
            echo 'Tuổi của bạn là:'. $_COOKIE['age'];
          }
          else {
            echo ' Cookie đã bị xóa';
          }
        ?>
    </h4>
    <div>
          <form method="POST">
            <input type="text" name="fullname"> <br>
            <input type="text" name="age"><br>
            <button type="submit" name="save-cookie">Save Cookie</button>
            <button type="submit" name='delete-cookie'>Xóa Cookie</button>
          </form>
    </div>
  </div>
  
</body>
</html>