<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1> Bài 09: Các Hàm xử lý mảng trong PHP </h1>
    <?php

      // Hàm array_change_key_case trong php để chuyển tất cả các key trong mảng sang chữ hoa
      echo '<h3> Hàm array_change_key_case trong PHP </h3>';
      $array = array(
        'chu_thuong' => 'Hello'
      );
      $array = array_change_key_case($array, CASE_UPPER);
      var_dump($array);

      // Hàm array_combine trong PHP để trộn 2 mảng bằng nhau thành mảng kết hợp
      echo '<h3> Hàm array_combine trong PHP </h3>';
      $array_keys = array('a', 'b', 'c');
      $array_values = array('one', 'two', 'three');
      echo 'Cho hài mảng:<br>';
      print_r($array_keys); 
      echo '<br>và<br>'; 
      print_r($array_values);
      echo 'Trộn 2 mảng:<br>';
      print_r(array_combine($array_keys, $array_values));

      // Hàm array_count_values trong PHP đếm số lần xuất hiện của các phần tử giống nhau trong mảng trả về 1 mảng kết quả
      echo '<h3> Hàm array_count_values trong PHP </h3>';
      $array = array(1, 'hello', 1, 'words', 'hello');
      echo 'cho mảng:';
      print_r($array);
      echo '<br>'.' số phần tử xuất hiện trong mảng';
      print_r(array_count_values($array));

      // Hàm array_push trong PHP thêm phần tử mới vào cuối mảng
      echo '<h3> Hàm array_push trong PHP </h3>';
      $stack = array('orange', 'apple');
      echo 'cho mảng:<br>';
      print_r($stack);
      echo 'Thêm 2 phần tử banana và banana vào mảng ';
      array_push($stack, 'banana', 'banana');

      // Hàm array_pop trong PHP xóa phần tử cuối trong mảng
      echo '<h3> Hàm array_pop trong PHP </h3>';
      $stack = array("orange", "banana", "apple", "raspberry");
      echo 'Cho mảng';
      print_r($stack);
      echo '<br>'. 'xóa phần tử cuối cùng trong mảng';
      $fruit = array_pop($stack);
      print_r($stack);
      
      // Hàm array_pad trong PHP 
      echo '<h3> Hàm array_pad trong PHP </h3>';
      $input = array(12,10,9);
      echo 'cho array:';
      print_r($input);
      echo '<br>'. 'Giãn 5 phần tử ở cuối mảng';
      $result = array_pad($input, 5, 0);
      echo 'mảng vừa dãn là:';
      print_r($result);

      echo '<br>'. 'Giãn 5 phần tử ở đầu mảng';
      $result = array_pad($input, -5, -1);
      echo 'mảng vừa dãn là:';
      print_r($result);


      // Hàm array_shift trong PHP xóa phần tử đầu tiên trong mảng và trả về phần tử vừa xóa
      echo '<h3> Hàm array_shift trong PHP </h3>';
      echo 'echo mảng $stack <br> ';
      print_r($stack);
      $fruit = array_shift($stack);
      echo '<br>'. 'kết quả:';
      print_r($fruit);


      // Hàm array_unshift thêm phần tử vào đầu mảng
      echo '<h3> Hàm array_unshift trong PHP </h3>';
      $queue = array('orange', 'banana');
      echo 'cho mảng queue <br>';
      print_r($queue);
      array_unshift($queue, 'apple', 'raspberry');
      echo '<br>'. 'thêm 2 phần tử vào mảng:';
      print_r($queue);
      
      // Hàm is_array trong php để kiểm tra đối tượng có phải là mảng hay ko?
      echo '<h3> Hàm is_array trong PHP </h3>';
      $arr = array(12,3,45,6,7,8,9);
      echo 'cho mảng arr:<br>';
      print_r($arr);
      echo '<br>';
      if(is_array($arr)) {
        echo 'Đối tượng $arr là mảng';
      } else {
        echo 'Đối tượng $arr không phải là mảng';
      }

      // Hàm in_array trong PHP  để kiểm tra giá trị có nằm trong mảng hay không và trả về true nếu có or ngược lại
      echo '<h3> Hàm in_array trong PHP </h3>';
      echo 'cho mảng arr:<br>';
      print_r($arr);
      echo 'kiểm tra 45 có trong mảng ko ? <br>';
      var_dump(in_array('45', $arr));

      // Hàm array_unique trong PHP để loại bỏ giá trị trùng trong mảng
      echo '<h3> Hàm array_unique trong PHP </h3>';
      $arr = array(12,3,45,6,7,1,3,12,8,9);
      echo 'cho mảng arr:<br>';
      print_r($arr);
      $result = array_unique($arr);
      echo 'kết quả:';
      print_r($result);

      // Hàm array_key_exit trong PHP kiểm tra key phần tử có tồn tại 
      echo '<h3> Hàm array_key_exit trong PHP </h3>';
      echo 'cho mảng arr:<br>';
      print_r($arr);
      echo '<br>';
      var_dump(array_key_exists('1', $arr));

      // hàm array_values trong PHP để chuyển mảng sang dạng chỉ mục
      echo '<h3> Hàm array_values trong PHP </h3>';
      $array = array(
        'username' => 'thehalfheart',
        'password' => 'somepasss'
      );
      echo 'cho mảng array:<br>';
      print_r($array);
      echo 'Chuyển mảng sang dạng chỉ mục: <br>';
      print_r(array_values($array));
    ?>
  </div>
</body>
</html>