<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
  echo '<h1> Bài 06: Xây dựng hàm trong PHP</h1>';
  echo 'number = 15';
  echo 'number = 20';
  echo '<br><br>';
  $number = 15;
  if(checkIsNumber($number)) {
    echo $number, 'là số chẵn';
  } else {
    echo $number, 'là số lẻ';
  }

  // hàm kiểm tra số chẵn lẻ
  function checkIsNumber($number) {
    if($number % 2 == 0)
      return true;
    else return false;
  }

  echo '<br>';
  // OR
  $number1 = 20;

  checIsBest($number, $number1);

  function checIsBest($num, $num1) {
    if ($num > $num1) {
      echo $num. 'là số lớn nhất';
    } else {
      echo $num1. 'là số lớn nhất';
    }
  }

  echo '<br>';
  // Biến toàn cúc với biến cục bộ
  $variable_global = 100;
  check();
  function check() {

    $variable_local = 20;
    global $variable_global;

    if ($variable_local % $variable_global) {
      echo 'chia hết';
    } else {
      echo 'chia lấy dư';
    }
  }

  echo '<br>';
  // truyền bằng giá trị
  echo '<h3>Truyền giá trị</h3>';
  $a = 1;
  function counter($a) {
    return $a + 1;
  }

  echo counter($a);

  echo '<h3>Truyền bằng tham chiếu</h3>';
  echo '<br>';
  // truyền bằng giá trị
  $a = 1;
  function counter(&$a) {
    return $a + 1;
  }

  echo counter($a);

  function A() {
    B();
  }
  function B() {
    C();
  }
  function C() {
    echo 'this is function C';
  }

  A();
  ?>
</body>
</html>