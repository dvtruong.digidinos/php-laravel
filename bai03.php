<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <?php
    echo '<h1>Bài 03: Toán tử và biểu thức, if else và switch case trong PHP';
    // Các toán tử +, -, *, / trong php
    echo 'Các toán tử +, -, *, / trong php';
    $a = 10;
    $b = 20;
    $c = $a + $b;
    echo 'cho a = 10';
    echo 'cho b = 20';
    echo ' Tổng a và b', $c;
    echo 'Hiệu a và b', $a + $b;
    echo 'Tich a và b', $a * $b;
    echo 'Thương a và b', $a / $b;

    echo '<br><br>'

    echo 'Các toán tử quan hệ trong PHP';
    if ($a > $b) {
      echo 'a > b';
    } elseif ($a >= $b) {
      echo 'a >= b';
    } elseif ($a < b) {
      echo 'a < b';
    } elseif ($a <= $b) {
      echo 'a <= b';
    }

    $a = null;
    $b = 10;
    if($a > 0 && $b > 0) {
      echo ' có a và b';
    } elseif($a || $b) {
      echo 'có a hoặc b';
    }
    echo '<br>';
    $a = 5;
    $b = 20;
    echo 'a = b đúng hay sai? ', $a === $b;
    echo 'a khac b đúng hay sai ?' $a != $b;

    echo '<br>'
    echo '<h3>Switch case:</h3>';
    $a = 1;
    switch($a) {
      case 1:
        echo 'a = 1';
        break;
      case 2: 
        echo 'a = 2';
        break;
      default: 
        echo 'Không tìm thấy a';
        break;
    };

  ?>
</body>
</html>