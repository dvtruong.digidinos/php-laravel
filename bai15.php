<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1>Bài 15: Filter, filter_var trong PHP</h1>
    <h3>Filter chuỗi</h3>
    <?php
      $str = '<h1> Hello World</h1>';
      $newStr = filter_var($str, FILTER_SANITIZE_STRING);
      echo $newStr;
    ?>

    <h3>Filter số nguyên</h3>
  <?php
      $int = 100;
      if (filter_var($int, FILTER_VALIDATE_INT)) {
          echo('Đây là số nguyên');
      } else {
          echo( 'Không phải số nguyên');
      }
    ?>

    <h3>Filter IP</h3>
    <?php
      $ip = '127.0.0.1';
      if (filter_var($ip, FILTER_VALIDATE_IP)) {
        echo "$ip là địa chỉ hợp lệ";
      } else {
        echo "$ip là địa chỉ không hợp lệ";
      }
    ?>
    <h3>Filter địa chỉ email</h3>
    <?php
      $email = 'truong.dv@digidinos.com';
      if (filter_var($email, FILTER_SANITIZE_EMAIL)) {
        echo "$email hợp lệ";
      } else {
        echo "$email không hợp lệ";
      }
    ?>
    <h3>Filter địa chỉ URL</h3>
    <?php
      $url = "https://www.w3school.com";
      if (filter_var($url, FILTER_SANITIZE_URL)) {
        echo "$url hợp lệ";
      } else {
        echo "$url không hợp lệ";
      }
    ?>
    <h3>Filter địa chỉ IPv6</h3>
    <?php 
      $ip = "2001:0db8:85a3:08d3:1319:8a2e:0370:7334";
      if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
        echo "$ip là một địa chỉ IPv6";
      } else {
        echo "$ip không phải là một địa chỉ IPv6";
      }
    ?>
    <h3>Filter địa chỉ URL và URL phải có chuỗi truy vấn</h3>
    <?php 
      $url = "https://www.w3school.com/?id=123213&method=get";
      if (filter_var($url, FILTER_SANITIZE_URL, FILTER_FLAG_QUERY_REQUIRED)) {
        echo "$url là url truy vấn";
      } else {
        echo "$url không phải là url truy vấn";
      }
    ?>
    <h3>Xóa tất cả ký tự có giá trị ASCII  > 127</h3>
    <?php
      $str = "<h1>Hello WorldÆØÅ!</h1>";
      $newstr = filter_var($str, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
      echo $newstr;
      ?>
  </div>
</body>
</html>