<!DOCTYPE html>
<html lang="en">
<head>
  <?php
    if (isset($_POST['back'])) {
      header('Content-Type: text/html; charset=utf-8');
      header('Content-type: text/javascript');
      header('mame:viewport; Content-type: width=device-width, initial-scale=1.0');
      // header('Location: http://localhost:8000/php-laravel');
    }
  ?>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1>Bài 14: Tìm hiểu về hàm header trong PHP</h1>
    <h3>1.Header điều hướng trang</h3>
    <form method="POST" action="">
      <button type="submit" name="back">Go to back</button>
    </form>
    <h3>2. Khắc phục lỗi font với hàm header</h3>
    <?php
      echo "header('Content-Type: text/html; charset=utf-8')";
    ?>
    <h3>3. Khai báo định dạng file</h3>
    <?php
      echo "header('Content-type: text/javascript')";
    ?>
  </div>
</body>
</html>