<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1> Bài 10: Các hàm kiểm tra kiểu dữ liệu trong PHP</h1>
    <!--  Kiểm tra biến có tồn tại ko? -->
    <h3> Hàm isset trong PHP</h3>
    <?php
      $var = 0;
      echo 'cho var = 0<br>';
      if (isset($var)) {
        echo 'var có tồn tại';
      } else {
        echo 'var không tồn tại';
      }
      
      // Hàm empty trong PHP
      echo '<h3> Hàm empty trong PHP</h3>';
      if (empty($var)) {
        echo 'var empty';
      } else {
        echo 'var is not empty';
      }

      // Hàm is_array trong PHP

      echo '<h3> Hàm is_array trong PHP</h3>';
      if (is_array($var)) {
        echo 'var is array';
      } else {
        echo 'var is not array';
      }

      // Hàm is_string trong PHP
      echo '<h3> Hàm is_string trong PHP</h3>';
      if (is_string($var)) {
        echo 'var is string';
      } else {
        echo 'var is not string';
      }
      
      // Hàm is_int trong PHP
      echo '<h3> Hàm is_int trong PHP</h3>';
      if (is_int($var)) {
        echo 'var is integer';
      } else {
        echo 'var is not integer';
      }

      // Hàm is_float trong PHP
      echo '<h3> Hàm is_float trong PHP</h3>';
      if (is_float($var)) {
        echo 'var is float';
      } else {
        echo 'var is not float';
      }

      // Hàm is_double trong PHP
      echo '<h3> Hàm is_double trong PHP</h3>';
      if (is_double($var)) {
        echo 'var is is_double';
      } else {
        echo 'var is not is_double';
      }

      // Hàm is_null trong PHP
      echo '<h3> Hàm is_null trong PHP</h3>';
      if (is_null($var)) {
        echo 'var is null';
      } else {
        echo 'var is not null';
      }
    ?>
  </div>
</body>
</html>