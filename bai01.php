<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
 <!-- Bài 01: Khai báo biến và hằng số trong PHP -->
  <?php
    // Chương trình "Hello world"
    echo '<h1>Bài 01:</h1> ';
    echo '<h2>1. Chương trình "Hello World"</h2>';
    echo '<h2>Hello world!</h2>';
    echo '<h3>Chào Mừng Các Bạn Đến Với Khóa Học PHP Cơ Bản Tại Digidinos</h3>';

    // Các loại comment trong PHP
     // Comment 1 dòng
     /**
      * Comment nhiều dòng
      */

    // Khai báo biến trong PHP
    echo '<br>';
    echo '<h2>2. Khai báo biến trong PHP</h2>';
    // Biến là kiểu số
    $a = 10;
    $b = 20;
    $c = 55.55;
    echo 'Cho biến a = 10 <br>';
    echo 'Cho biến b = 20 <br>';
    echo 'Cho biến c = 55.55 <br>';
    echo 'Tổng của a và b là:', $a + $b;
    echo 'Tổng của a, b, c là:', $a + $b + $c;

    echo '<br><br>';
    // Biến là chuỗi
    $str = 'Chào Mừng Các Bạn Đến Với Khóa Học PHP.';
    echo 'Khai báo giá trị của biến "str" là một chuỗi.';
    echo ' str:', $str;

    echo '<br><br>';
    // Biến kiểu boolean
    $a = true;
    $b = false;

    echo 'Giá trị của a là:';
    var_dump($a);
    echo '<br>';
    echo 'Giá trị của b là:';
    var_dump($b);

    echo '<br>';

    echo '<h2>2. Khai Báo Hằng Trong PHP</h2>';
    // Khai báo hằng
    define('name', 'Dinh Van Truong');
    define('age', 22);
    echo '<br>';
    echo 'Name:', name, '<br>';
    echo 'Age:', age;
  ?>
</body>
</html>