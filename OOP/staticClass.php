<?php
  class conNguoi {
    // biến local chỉ có thể được gọi trong class
    private static $name = "Hoi Lam Gi";

    public static function setName($name) {
      // self:: gọi đến các thuộc tính static trong class
      return self::$name = $name;
    }
    public static function getName() {
      return self::$name;
      //or
      return conNguoi::$name;
    }
    public static function showName() {
      return self::getName();
      //or
      return conNguoi::getName();
    }
  }

//  $a = conNguoi::$name;
  $a = conNguoi::showName();
  conNguoi::setName('DINH VAN TRUONG');
  $b = conNguoi::showName();
  echo $a;
  echo '<br>'. $b;

  class nguoiLon extends conNguoi {
    public static function getName() {
      return conNguoi::showName();
    }
  }

  $c = nguoiLon::getName();
  nguoiLon::setName('NGUYEN VAN A');
  $d = nguoiLon::showName();
  echo '<br><br><h3>'. $c.'</h3>';
   echo '<br><br><h3>'. $d.'</h3>';
?>