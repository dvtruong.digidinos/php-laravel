<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1>Bài 13: Xử lý ngày tháng trong PHP</h1>
    <h3>1. Thiết lập time_zone tại Việt Nam</h3>
    <div>
      <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        // Xem danh sách time zone
        $timeZone = DateTimeZone::listIdentifiers();
        foreach ($timeZone as $item) {{
          echo '<span>'.$item .'</span><br>';
        }}
      ?>
      <h3>2. Định dạng ngày tháng với hàm date trong PHP</h3>
      <?php
      date_default_timezone_set('Asia/Ho_Chi_Minh');
      echo "<h3>Current Time:</h3>". date('d/m/Y - H:i:s');
      ?>
      <!-- Xử lý thời gian nâng cao -->
      <h3>3. Xử lý ngày tháng nâng cao trong PHP</h3>
      <?php 
        $a = 'bây giờ là H giờ';
        $time = addcslashes($a, 'a..z');
        echo date($time);

        // chuyển time sang kiểu int
        echo '<h4>Chuyển time sang INT</h4>';
        echo 'Sử dụng hàm strtotime: ';
        echo strtotime(date('Y-m-d H:i:s'));
      ?>
    </div>
    <h3>4. Xử lý cộng trừ ngày tháng với mktime</h3>
    <?php
      $dateInt = mktime(0,0,0,11,(20+12), 2019);
      echo date('d/m/Y', $dateInt);

      echo 'Xem ngày tới, tháng tới , năm tới<br>';
      $tomorrow = mktime(0,0,0, date("m"), date("d")+1, date("Y"));
      echo 'Tomorrow:'.' '. $tomorrow.'<br>';
      $lastMonth = mktime(0,0,0, date('m')-1, date('d'), date('Y'));
      echo 'Last Month: '. ' '.$lastMonth.'<br>';
      $nextYear = mktime(0,0,0, date('m'), date('d'), date('Y')+1);
      echo 'Next year:'.' '.$nextYear;
    ?>
  </div>
</body>
</html>