<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <div>
    <h1>Bài 08: Các hàm xử lý chuỗi trong PHP</h1>
  </div>
  <?php

    // Hàm addcslashes trong PHP đùng để thêm những ký tự (',",\) tron chuỗi
    echo "<h3>Hàm addcslashes trong PHP</h3>";
    $str = " Khóa học PHP cơ bản";
    echo (addcslashes($str, 'a..z'));

    echo '<br/>';
    echo '<h3> Hàm stripslashes trong PHP</h3>';
    // Hàm stripslashes trong php đùng để xóa các đâu (\ trong chuỗi)
    echo (stripslashes("Mot\so ham\'xu ly chuoi'\ trong PHP"));
    echo '<br>';

   // Hàm crc32 trong PHP chuyển chuỗi thành số nguyên hoặc custom
    echo '<h3>Hàm crc32 trong PHP</h3>';
    $str1 = "Công ty cổ phần Digidinos";
    echo 'Cho chuỗi $str1 = "Digidinos"';
    echo crc32($str);

    // Hàm explode trong php để chuyển string thành mảng các phần tử với ký tự tách mảng
    echo '<h3> Hàm explode trong PHP</h3>';
    var_dump(explode(' ',$str1));

    // hàm implode trong PHP để đổ mảng thành chuỗi bằng $delimiter
    echo '<h3> Hàm implode trong PHP</h3>';
    echo implode(' ', array('Xin', 'chao', 'các', 'bạn!'));

    //Hàm ord trong PHP trả về mà ASCII của ký tự đầu tiên trong chuỗi
    echo '<h3> Hàm ord trong PHP</h3>';
    echo ord('CdAbcddd');

    // Hàm strlen trong php để đếm số ký tự trong chuỗi
    echo '<h3> Hàm strlen trong PHP</h3>';
    echo 'Số ký tự trong chuỗi là';
    echo strlen($str1);

    // Hàm str_ word_count trong PHP để trả về số từ trong chuỗi
     echo '<h3> Hàm str_word_count trong PHP</h3>';
    echo 'Số từ trong chuỗi $str1:';
    echo str_word_count($str1);

    // Hàm str_repeat trong PHP để lặp chuỗi
    echo '<h3> Hàm str_repeat trong PHP</h3>';
    echo 'Lặp chuỗi str1 ba lần';
    echo str_repeat($str1, 3);

    // Hàm replace trong PHP để tìm kiếm thay thế chuỗi
    echo '<h3> Hàm str_replace trong PHP</h3>';
    echo "thay đế chữ 'ty' trong str1";
    echo str_replace('ty','Ty', $str1);

    // Hàm md5 trong PHP mã hóa chuỗi thành dãy 32 ký tự
    echo '<h3> Hàm md5 trong PHP</h3>';
    echo 'mã hóa md5 chuỗi str1';
    echo md5($str1);

    // Hàm sha1 trong PHP để mã hóa chuỗi thành dãy 40 ký tự
    echo '<h3> Hàm sha1 trong PHP</h3>';
    echo 'mã hóa sha1 chuỗi str1:';
    echo sha1($str1);

    // Hàm htmlentities trong php để chuyển các thẻ html sang dạng thực thể của chungs
    echo '<h3> Hàm htmlentities trong PHP</h3>';
    echo 'Chuyển thẻ html sang dạng thực thể';
    echo htmlentities('<h1>Bài học PHP tại DIGIDINOS</h1>');

    // Hàm html_entity_decode trong php để chuyển ngược các ký tự dạng thực thể HTML sang dạng ký tự của chúng
    echo '<h3> Hàm html_entity_decode trong PHP</h3>';
    $str2 = htmlentities('<h1>Bài học PHP tại DIGIDINOS</h1>');
    echo 'Entity:'. $str2. '<br>';
    echo 'Decode:'. html_entity_decode($str2);

    // hàm htmlspecialchars trong PHP: tương tự htmlentities.
    echo '<h3> Hàm htmlspecialchars trong PHP</h3>';
    echo 'Chuyển thẻ html sang dạng thực thể';
    echo htmlspecialchars('<h1>Bài học PHP tại DIGIDINOS</h1>');
    
    // Hàm htmlspecialchars_decode trong php để chuyển ngược các ký tự dạng thực thể HTML sang dạng ký tự của chúng
    echo '<h3> Hàm htmlspecialchars_decode trong PHP</h3>';
    $str2 = htmlentities('<h1>Bài học PHP tại DIGIDINOS</h1>');
    echo 'Entity:'. $str2. '<br>';
    echo 'Decode:'. htmlspecialchars_decode($str2);

    // Hàm strip_tags trong PHP để bỏ các thẻ html trong chuỗi
    echo '<h3> Hàm strip_tags trong PHP</h3>';
    echo strip_tags('<b>Digidinos</b>', 'b');

    // Hàm substr trong PHP lấy 1 chuỗi con nằm trong chuỗi
    echo '<h3> Hàm substr trong PHP</h3>';
    echo substr($str1, 0, 11);

    // Hàm strstr trong PHP tách chuỗi từ ký tự cho trước đế hết chuỗi
    echo '<h3> Hàm strstr trong PHP</h3>';
    echo strstr($str1, 'cổ');

    // Hàm strpos trong php để tìm vị trí trong chuỗi
    echo '<h3> Hàm strpos trong PHP</h3>';
    echo 'Vị trí của chữ Digidinos trong str1 là:';
    echo strpos($str1, 'Digidinos');
    
    // Hàm strtolower và strtoupper trong php để chuyển chuỗi thành chữ hoa và thường
    echo '<h3> Hàm strtolower và strtoupper trong PHP</h3>';
    echo 'Chuyển đổi chỗi str1 thành chữ hoa';
    echo strtoupper($str1);
    echo 'Chuyển đổi chỗi str1 thành chữ thường';
    echo strtolower($str1);


    // Hàm ucfirst trong php chuyển ký tự đầu tiên trong chuỗi thành chữ hoa
    echo '<h3> Hàm ucfirst trong PHP</h3>';
    echo ucfirst($str1);

    // Hàm ucwords trong php chuyển chữ đầu tiên trong chuỗi thành chữ hoa
    echo '<h3> Hàm ucwords trong PHP</h3>';
    echo ucwords($str1);

    // Hàm trim trong PHP dùng để xóa ký tự ở đầu hoặc cuối nếu không nhập ký tự là khoảng trắng trong chuỗi(nếu ko nhận ký tự)
    echo '<h3> Hàm trim trong PHP</h3>';
    echo 'Xóa khoảng trắng trong chuỗi str1';
    echo '<br>';
    echo trim($str1);
    echo ltrim($str1); // xóa bên trái;
    echo rtrim($str1); // xóa bên phải;


    echo '<h3> Hàm nl2br trong PHP</h3>';
    echo nl2br('\n công ty cổ phần Digidinos');


    echo '<br>';
    echo '<br>';
    // Hàm json_decode trong PHP để chuyển JSON sang các đổi tượng mảng hoặc object 
    echo '<h3> Hàm json_encode trong PHP</h3>';
    $json = array("id" => '123', "name"=> "Nguyễn Văn A", "age"=>'22');
    print_r($json);
    echo ' chuyển mảng sang json<br>';
    echo json_encode($json);
  ?>
</body>
</html>