<?php
  // Bài học orverride method and properties
  class MyClass
  {
    public $prop1 = "I'm a class property!";
  
    public function __construct() {
        echo 'The class "', __CLASS__, '" was initiated!<br />';
    }
  
    public function __destruct() {
        echo 'The class "', __CLASS__, '" was destroyed.<br />';
    }
  
    public function __toString() {
        echo "Using the toString method: ";
        return $this->getProperty();
    }
  
    public function setProperty($newval) {
        $this->prop1 = $newval;
    }
  
    public function getProperty() {
        return $this->prop1 . "<br />";
    }
  }

  // class thừa kế từ 1 lớp khác có thể sử dụng được các thuộc tính, hàm của class kế thừa
  class MyOtherClass extends MyClass
  {
    public function newMethod()
    {
        echo "From a new method in " . __CLASS__ . ".<br />";
    }
  }
  // Khởi tạo object mới
  $newobj = new MyOtherClass;
  echo $newobj->newMethod();
  echo $newobj->getProperty();
?>